import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

class RentingGars extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ro: localStorage.getItem('ro'),
            en: localStorage.getItem('en')
        };
        this.setStates = this.setStates.bind(this);
    }

    setStates(){
        this.setState({ro: localStorage.getItem('ro')})
        this.setState({en: localStorage.getItem('en')})
    }
    render() {
        const hs = {
            marginLeft: "30px",
            marginTop: "30px",
            textAlign: "justify"
        };
        const ps = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const imgs = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const columns = {
          float: 'left',
          width: '32%',
          padding: '5px'
        }

        const rows = {
          content: "",
          clear: 'both',
          display: 'table'
        }
        if(this.state.ro === 'true'){
        return(
        <div className = "RentingGarsClass">
            <h1 style={hs}>Inchirieri garsoniere</h1>
            	<br /><br /><br />
            	<p style={ps}>    Aici veti putea gasi ofertele noastre de garsoniere pentru inchiriere.
            	</p>
            	<br />
            	<p style={ps}>Toate garsonierele propuse spre inchiriere sunt complet mobilate si utilate.</p>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/gars1.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 15mp</p>
            			<p style={ps}>Pret/luna: 150 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars2.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 23mp</p>
            			<p style={ps}>Pret/luna: 200 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars3.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Manastur, 22mp</p>
            			<p style={ps}>Pret/luna: 220 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/gars4.jpg")} width="400" height="300" />
            			<p style={ps}>Centru, 20mp</p>
            			<p style={ps}>Pret/luna: 230 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars5.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Buna Ziua, 25mp</p>
            			<p style={ps}>Pret/luna: 190 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars6.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Intre Lacuri, 21mp</p>
            			<p style={ps}>Pret/luna: 200 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/gars6.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 19mp</p>
            			<p style={ps}>Pret/luna: 180 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars7.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 25mp</p>
            			<p style={ps}>Pret/luna: 210 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars8.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Iris, 22mp</p>
            			<p style={ps}>Pret/luna: 170 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            </div>
         )
         }
          else if(this.state.en === 'true') {
                return(
                        <div className = "RentingGarsClass">
                            <h1 style={hs}>Renting studios</h1>
                            	<br /><br /><br />
                            	<p style={ps}>    Here you can find our offers for studios for rent.
                            	</p>
                            	<br />
                            	<p style={ps}>All studios offered for rent are fully furnished and equipped.</p>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/gars1.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Someseni, 15mp</p>
                            			<p style={ps}>Price/month: 150 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars2.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Zorilor, 23mp</p>
                            			<p style={ps}>Price/month: 200 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars3.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Manastur, 22mp</p>
                            			<p style={ps}>Price/month: 220 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/gars4.jpg")} width="400" height="300" />
                            			<p style={ps}>Centru, 20mp</p>
                            			<p style={ps}>Price/month: 230 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars5.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Buna Ziua, 25mp</p>
                            			<p style={ps}>Price/month: 190 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars6.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Intre Lacuri, 21mp</p>
                            			<p style={ps}>Price/month: 200 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/gars6.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Someseni, 19mp</p>
                            			<p style={ps}>Price/month: 180 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars7.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Zorilor, 25mp</p>
                            			<p style={ps}>Price/month: 210 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars8.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Iris, 22mp</p>
                            			<p style={ps}>Price/month: 170 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            </div>
                         )

          }
    };
}

export default RentingGars