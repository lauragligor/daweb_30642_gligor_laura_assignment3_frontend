import React from 'react'
import {BrowserRouter, Route, Routes} from 'react-router-dom'
import style from './css/style.css';
import News from './news'
import Home from './home'
import AboutUs from './aboutus'
import RentingGars from './rentinggars'
import RentingApart from './rentingapart'
import RentingHouse from './rentinghouse'
import SellingGars from './sellinggars'
import SellingApart from './sellingapart'
import SellingHouse from './sellinghouse'
import Contact from './contact'
import Login from './login'
import Register from './register'
import Client from './client'
import Agent from './agent'

const App = () => {
        return(

            <BrowserRouter>
            <Routes>
                    <Route
                        exact
                        path='/'
                        element={<Home/>}
                    />
                    <Route
                        exact
                        path='/home'
                        element={<Home/>}
                    />
                    <Route
                        exact
                        path='/news'
                        element={<News/>}
                    />
                    <Route
                        exact
                        path='/aboutus'
                        element={<AboutUs/>}
                    />
                    <Route
                        exact
                        path='/login'
                        element={<Login/>}
                    />
                    <Route
                        exact
                        path='/client'
                        element={<Client/>}
                    />
                    <Route
                        exact
                        path='/agent'
                        element={<Agent/>}
                    />
                    <Route
                        exact
                        path='/register'
                        element={<Register/>}
                    />
                    <Route
                        exact
                        path='/contact'
                        element={<Contact/>}
                    />
                    <Route
                        exact
                        path='/rentinggars'
                        element={<RentingGars/>}
                    />
                    <Route
                        exact
                        path='/rentingapart'
                        element={<RentingApart/>}
                    />
                    <Route
                        exact
                        path='/rentinghouse'
                        element={<RentingHouse/>}
                    />
                    <Route
                        exact
                        path='/sellinghouse'
                        element={<SellingHouse/>}
                    />
                    <Route
                        exact
                        path='/sellingapart'
                        element={<SellingApart/>}
                    />
                    <Route
                        exact
                        path='/sellinggars'
                        element={<SellingGars/>}
                    />
                </Routes>
            </BrowserRouter>

        )

}

export default App;
