import React, {useState, useEffect} from "react";
import {useNavigate} from "react-router-dom";

function Login(){

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();
    let role = "";

    useEffect(() => {
        if (localStorage.getItem('user-info')){
            navigate("/home");
        }
    }, [])

    async function logIn(){
        let item = {email, password};
        console.log(item);
        let result = await fetch("http://localhost:8000/api/login", {
            method: 'POST',
            body: JSON.stringify(item),
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        })
        result = await result.json();
        let err = "error";
        console.log(JSON.stringify(result));
        if(JSON.stringify(result) !== err) {
            localStorage.setItem("user-info", JSON.stringify(result));
            let item = localStorage.getItem('user-info');
            let obj = JSON.parse(item);
            role = obj.role;
            if(role == "client") {
                navigate("/client");
            }
            else{
                navigate("/agent");
            }
        }
    }
    return(
        <div className="col-sm-6 offset-sm-3">
            <h1>Login Page</h1>
            <input type="text" value={email} onChange={(e) => setEmail(e.target.value)} className="form-control" placeholder="email"/>
            <br/>
            <input type="password" value={password} onChange={(e) => setPassword(e.target.value)} className="form-control" placeholder="password"/>
            <br/>
            <button onClick={logIn} className="btn btn-primary">Log In</button>
        </div>
    )
}
export default Login;